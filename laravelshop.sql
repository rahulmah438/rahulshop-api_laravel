-- MySQL dump 10.13  Distrib 5.5.61, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: laravelshop
-- ------------------------------------------------------
-- Server version	5.5.61-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `laravelshop`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `laravelshop` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `laravelshop`;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` varchar(30) NOT NULL,
  `category` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES ('1','men','0000-00-00 00:00:00','0000-00-00 00:00:00'),('2','women','0000-00-00 00:00:00','0000-00-00 00:00:00'),('3','kids','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedbacks`
--

DROP TABLE IF EXISTS `feedbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedbacks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `subj` varchar(30) NOT NULL,
  `mesg` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedbacks`
--

LOCK TABLES `feedbacks` WRITE;
/*!40000 ALTER TABLE `feedbacks` DISABLE KEYS */;
INSERT INTO `feedbacks` VALUES (1,'Rahul','33333333333','rahul@gmail.com','service','try to improve ur service','2018-09-24 03:35:59','0000-00-00 00:00:00'),(2,'Rahul','33333333333','rahul@gmail.com','service','try to improve ur service','2018-09-24 03:36:13','0000-00-00 00:00:00'),(3,'Rahul','33333333333','rahul@gmail.com','good, impressive','Your service is very good and ','2018-09-24 03:36:47','0000-00-00 00:00:00'),(4,'Akash','09013628187','akash.jindal@kelltontech.com','Good Service','FeedbackForm','2018-09-25 02:25:10','2018-09-25 02:25:10');
/*!40000 ALTER TABLE `feedbacks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `itemno` varchar(30) NOT NULL,
  `catg` varchar(40) NOT NULL,
  `subcatg` varchar(40) NOT NULL,
  `price` varchar(30) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `info` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'1','2','Dresses','1899','Semi Formal Dress','Fabric Detailing Cap Sleeves Open Bust Button','2018-09-24 13:30:39','2018-09-24 08:00:39'),(2,'d2','2','Dresses','1699','Casual Dress','Viscose Blend Fabric Smoked Back Smart Shift Dress Sleeveless Linen Shift Dress','2018-09-22 11:53:03','0000-00-00 00:00:00'),(3,'d3','2','Dresses','1799','Formal dress','Pin Striped Incut Dress Polyester Fabric Sleeveless  ','2018-09-22 11:53:09','0000-00-00 00:00:00'),(4,'d4','2','Dresses','1649','Formal Dress','Buttoned Down Front Linen Fabric Sleeveless','2018-09-22 11:53:15','0000-00-00 00:00:00'),(5,'cs1','1','Casual Shirts','1999','United Colors of Benetton Mens Full Sleeves Checks Shirt','100% Cotton Full Sleeves Smart Checks Earthy Foliage Combination','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'cs2','1','Casual Shirts','1999','United Colors of Benetton Mens Full Sleeves Checks Shirt','100% Cotton Full Sleeves Stripe Shirt Light Weight Fabric','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'cs3','1','Casual Shirts','1999','United Colors of Benetton Mens Full Sleeves Checks Shirt\r\n','Bias Yoke Patch Pockets Slim Fit Half Sleeves','2018-09-23 21:35:46','0000-00-00 00:00:00'),(8,'cs4','1','Casual Shirts','1499','Allen Solly Mens Full Sleeves Checks Sport Shirt','Bias Yoke Patch Pockets Slim Fit Half Sleeves','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'cs5','1','Casual Shirts','1999','Levis Mens Roll Up Sleeves Workers Shirt','Light Age Theme 100% Cotton Full Sleeves Checks Shirt White Base','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'cs6','1','Casual Shirts','1599','Mustang Mens Long Sleeves Mini Checks Shirt','Summer Colors Theme 100% Cotton Full Sleeves Bias Detailing at Side Seam','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'s1','2','Churidar Suits','1699','BIBA Churidar - Kurta-Dupatta Set','Printed kurta Round neck Full sleeves Kurta length -40 inches Contrast churidar and shaded crushed Duppatta','2018-09-22 12:05:46','0000-00-00 00:00:00'),(12,'s2','2','Churidar Suits','2599','Kashish Churidar - Kurta-Dupatta Set','Floral print Embelished Yoke Puff sleeves Kurta length -40 inches Contrast churidar and Duppatta','2018-09-22 12:05:55','0000-00-00 00:00:00'),(13,'s3','2','Churidar Suits','2899','Kashish Churidar - Kurta-Dupatta Set','Textured Fabric V-neck 3/4th sleeves Kurta length -40 inches Contrast churidar and Duppatta','2018-09-22 12:06:02','0000-00-00 00:00:00'),(14,'s4','2','Churidar Suits','2799','Seven East Churidar - Kurta-Dupatta Set','Textured Fabric Embelished Yoke Sleeveless Kurta length -38 inches Contrast churidar and Duppatta','2018-09-22 12:06:08','0000-00-00 00:00:00'),(15,'s5','2','Churidar Suits','2799','Kashish Churidar - Kurta-Dupatta Set','Printed kurta Round neck Half sleeves Kurta length -38 inches Contrast churidar and Printed Duppatta','2018-09-22 12:06:16','0000-00-00 00:00:00'),(16,'s6','2','Churidar Suits','2299','BIBA Churidar - Kurta-Dupatta Set','Regular fit Round neck Full sleeves Kurta length -40 inches Contrast churidar and shaded crushed Duppatta   ','2018-09-22 12:06:23','0000-00-00 00:00:00'),(17,'foot1','2','Sandals','1690','Tresmode Ladies Peep toes','Tresmode Ladies Peep toes','2018-09-22 12:30:38','2018-09-22 12:30:38'),(18,'foot2','2','Sandals','1690','Tresmode Ladies Pump Shoes','Tresmode Ladies Pump Shoes','2018-09-22 12:30:26','2018-09-22 12:30:26'),(19,'foot3','2','Sandals','4990','Tresmode Ladies Sandals','Tresmode Ladies Sandals','2018-09-22 12:30:13','2018-09-22 12:30:13'),(20,'foot4','2','Sandals','1299','Haute Curry - Ladies Footwear\r\n','Haute Curry - Ladies Footwear','2018-09-22 12:29:56','2018-09-22 12:29:56'),(21,'foot5','2','Sandals','1399','Silver Pepper - Ladies Footwear\r\n','Silver Pepper - Ladies Footwear','2018-09-22 12:29:39','2018-09-22 12:29:39'),(22,'foot6','2','Sandals','1499','Lemon Pepper - Ladies Footwear\r\n','Lemon Pepper - Ladies Footwear','2018-09-22 12:29:00','2018-09-22 12:29:00'),(23,'k1','2','Kurtas','1399','Haute curry Mix and Match Kurta','Tie up 5/8th sleeves Kurta Length - 38 inches Tribal printed 100% cotton  ','2018-09-22 12:14:37','0000-00-00 00:00:00'),(24,'k2','2','Kurtas','1079','W Mix and match kurta\r\n','Sleeveless Regular Wear Regular Fit Length- 38 inches Fabric- Cotton Cambric','2018-09-22 12:14:45','0000-00-00 00:00:00'),(25,'k3','2','Kurtas','1599','Kashish Mix and Match Kurta','3/4th sleeves Kurta Length - 38 inches sequine highlighted printed 100% cotton','2018-09-22 12:14:52','0000-00-00 00:00:00'),(26,'k4','2','Kurtas','799','W Mix and Match Kurta','V Neck Kurta Printed Short Sleeves Mauve printed non embellished Kurta Kurta length -38 inches 60 s Cambric','2018-09-22 12:15:00','0000-00-00 00:00:00'),(27,'k5','2','Kurtas','1199','Stop Classic Mix and Match Short Kurta\r\n','Dobby kurta with embroidery on the yoke and sleeve Kurta length- 36 inches 3/4th sleeves Officewear','2018-09-22 12:15:08','0000-00-00 00:00:00'),(28,'k6','2','Kurtas','1299','Stop Classic Mix and Match Short Kurta\r\n','Printed Highlighted placket and sleeve hem 3/4th sleeves','2018-09-22 12:15:17','0000-00-00 00:00:00'),(29,'d5','2','Dresses','1799','Life Ladies Rara Dress\r\n','Life Ladies Rara Dress','2018-09-24 08:35:05','0000-00-00 00:00:00'),(30,'d6','2','Dresses','1599','Mustang Ladies Mia Denim dress\r\n','Mustang Ladies Mia Denim dress','2018-09-24 08:35:13','0000-00-00 00:00:00'),(31,'of1','2','Office Wear','799','Austin Reed Office Wear T shirt','Austin Reed Office Wear T shirt','2018-09-24 08:35:21','0000-00-00 00:00:00'),(32,'of2','2','Office Wear','1999','Austin Reed Office Wear waist coat','Austin Reed Office Wear waist coat','2018-09-24 08:35:29','0000-00-00 00:00:00'),(33,'of3','2','Office Wear','799','Austin Reed Office Wear Top','Austin Reed Office Wear Top','2018-09-24 08:35:37','0000-00-00 00:00:00'),(34,'of4','2','Office Wear','899','Austin Reed Office Wear T shirt','Austin Reed Office Wear T shirt','2018-09-24 08:35:46','0000-00-00 00:00:00'),(35,'of5','2','Office Wear','1199','Austin Reed Office Wear T shirt','Austin Reed Office Wear T shirt','2018-09-24 08:35:53','0000-00-00 00:00:00'),(36,'of6','2','Office Wear','699','SHOP Office wear collection','SHOP Office wear collection','2018-09-24 08:36:01','0000-00-00 00:00:00'),(37,'Jewellery1','2','Artificial Jewellery','20865','Haute Curry Earring HCSE1012','Haute Curry Earring HCSE1012','2018-09-23 08:46:33','2018-09-23 07:47:32'),(38,'Jewellery2','2','Artificial Jewellery','10999','Lucera Rhodium plated Sterling Silver Diamond ring','Lucera Rhodium plated Sterling Silver Diamond ring','2018-09-23 08:46:41','2018-09-23 07:47:18'),(39,'Jewellery3','2','Artificial Jewellery','20345','Pretty Women Peacock theme Set ','Pretty Women Peacock theme Set ','2018-09-23 08:46:51','2018-09-23 07:47:54'),(40,'Jewellery4','2','Artificial Jewellery','23678','Pretty Women dangling Earrings ','Pretty Women dangling Earrings ','2018-09-23 08:46:59','2018-09-23 07:48:12'),(41,'Jewellery5','2','Artificial Jewellery','22780','DITI Flower theme Diamond Ring ','DITI Flower theme Diamond Ring ','2018-09-23 08:47:11','2018-09-23 07:48:29'),(42,'Jewellery6','2','Artificial Jewellery','24890','Pretty Women Modern theme set ','Pretty Women Modern theme set ','2018-09-23 08:47:19','2018-09-23 07:48:57'),(43,'bb1','3','Baby Apparel','899','Girls Spot and Stripe Jersey Romper Dress','Floral print Sleeveless Round neck Along with belt Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:17:48','2018-09-23 08:17:48'),(44,'bb2','3','Baby Apparel','1299','Girls Denim Pinny','Floral print denim dress Sleeveless Round neck Along with a belt Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:46:24','2018-09-23 08:18:04'),(45,'bb3','3','Baby Apparel','1299','Girls frill and pompy short Dress','Polka dot Strappy Square neck Double layered dress Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:47:28','2018-09-23 08:18:20'),(46,'bb4','3','Baby Apparel','1799','Girls Layette Snowsuit','Girls Layette Snowsuit','2018-09-24 08:34:53','2018-09-23 08:18:35'),(47,'bb5','3','Baby Apparel','899','Unisex Duck Pyjamas','Round Neck Half Sleeves Stripes With Print Button Styling Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:18:52','2018-09-23 08:18:52'),(48,'bb6','3','Baby Apparel','1299','Boys Truck Bodysuits - 7pk','Round Neck Half Sleeves Stripes Button Styling Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:47:47','2018-09-23 08:19:06'),(49,'g1','3','Girls Apparel','999','Gini and Jony girls dress (Infant)',' Sleeveless Balloon dress Striped at the waist Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:24:46','2018-09-23 08:24:46'),(50,'g2','3','Girls Apparel','445','612 Ivy League girls dress',' Floral print, Strappy, Elasticised at the hem, Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:24:59','2018-09-23 08:24:59'),(51,'g3','3','Girls Apparel','699','Girls floral dress','Floral print Strappy Elasticized body Frills at the hem Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:23:58','2018-09-23 08:23:58'),(52,'g4','3','Girls Apparel','499','Shop girls top','Stop girls top Product Details Floral print Halter Bow at the side Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:24:09','2018-09-23 08:24:09'),(53,'g5','3','Girls Apparel','549','Shop girls skirt',' Floral print Halter neck V-neck Bow at the front Frills at the bottom Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:24:20','2018-09-23 08:24:20'),(54,'g6','3','Girls Apparel','549','Shop girls casual top','Polka dot Puff sleeves Collar Polka dot print belt Front pleat Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:24:31','2018-09-23 08:24:31'),(55,'b1','3','Boys Apparel','499','United Colors of Benetton boys t-shirt','Half sleeves Printed message &#39;St.Kilda surfer&#39; Round neck Regular fit Prices of the items may be different from those displayed on the product details page, where the price varies by size.  ','2018-09-23 08:21:37','2018-09-23 08:21:37'),(56,'b2','3','Boys Apparel','399','United Colors of Benetton boys t-shirt','Half sleeves Printed message &#39;Art village 2&#39; Round neck Regular fit Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:21:48','2018-09-23 08:21:48'),(57,'b3','3','Boys Apparel','799','Gini and Jony boys T-shirt (Kids)','Printed tee Half sleeves Polo neck Printed baseball logo Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:22:00','2018-09-23 08:22:00'),(58,'b4','3','Boys Apparel','545','612 Ivy League boys shirt','Checks shirt Half sleeve with turnup loop Along with a tee Printed tee and shirt Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:22:11','2018-09-23 08:22:11'),(59,'b5','3','Boys Apparel','745','612 Ivy League boys T-shirt','Striped shirt Half sleeves Patch on the front Contrast collar and sleeve hem Prices of the items may be different from those displayed on the product details page, where the price varies by size.','2018-09-23 08:22:23','2018-09-23 08:22:23'),(60,'b6','3','Boys Apparel','879','Nike boys t-shirt','Solid color tee Sleeveless Round neck Printed logo \'64\' Prices of the items may be different from those displayed on the product details page, where the price varies by size','2018-09-23 08:22:40','2018-09-23 08:22:40'),(61,'Toy1','3','Kids Toys','525','Wild Republic Rascals Dolphin 20 inch soft toy','Simple Cute fluffy and adorable Non Toxic Quality Fabric Ultra Squishy Stuffing 20 inches Hand Crafted','2018-09-23 08:26:40','2018-09-23 08:26:12'),(62,'Toy2','3','Kids Toys','2499','Little Mommy Play All Day','Doll','2018-09-23 08:26:30','2018-09-23 08:26:30'),(63,'Toy3','3','Kids Toys','1649','Road Storage Mat R','A fantastic backdrop for road adventures and a practical storage case in one Race your toy cars on the playmat then fold the playmat into a storage box and place your favourite toys in it Water resistant wipe-clean surface Age Range: From 3 years Social skills-This toy helps your child learn how to make friends and enjoy company. Imagination-This toy encourages your child to enjoy using their imagination.','2018-09-23 08:26:55','2018-09-23 08:26:55'),(64,'Toy4','3','Kids Toys','549','Paper Straw Pets','Use your imagination to create your very own straw pets and a house for them too Age Range: From 3 years Creativity-This toy enables your child to express themselves artistically. Imagination-This toy encourages your child to enjoy using their imagination.','2018-09-23 08:27:05','2018-09-23 08:27:05'),(65,'Toy5','3','Kids Toys','1999','Disney Rapunzel hair braider','Manually operated Rapunzel doll','2018-09-23 08:27:38','2018-09-23 08:27:38'),(66,'Toy6','3','Kids Toys','699','Paper Aquarium','Have wonderful adventures with these dazzling paper fish and funky fish tank. Age Range: From 3 years Imagination-This toy encourages your child to enjoy using their imagination. Creativity-This toy enables your child to express themselves artistically.','2018-09-23 08:27:50','2018-09-23 08:27:50'),(67,'ts1','1','T-shirts','1699','United Colors of Benetton Mens Half Sleeves Polo Striper T-Shirt','United Colors of Benetton Mens Half Sleeves Polo Striper T-Shirt','2018-09-24 08:29:53','0000-00-00 00:00:00'),(68,'ts2','1','T-shirts','1099','United Colors of Benetton Mens Half Sleeves Polo T-Shirt1','United Colors of Benetton Mens Half Sleeves Polo T-Shirt1','2018-09-24 08:30:03','0000-00-00 00:00:00'),(69,'ts3','1','T-shirts','1299','United Colors of Benetton Mens Half Sleeves Polo T-Shirt','United Colors of Benetton Mens Half Sleeves Polo T-Shirt','2018-09-24 08:30:10','0000-00-00 00:00:00'),(70,'ts4','1','T-shirts','1099','Spykar Mens Half Sleeve Collar Neck Flat Knit T-Shirt','Spykar Mens Half Sleeve Collar Neck Flat Knit T-Shirt','2018-09-24 08:30:19','0000-00-00 00:00:00'),(71,'ts5','1','T-shirts','749','Spykar Mens Half Sleeve Round Neck Printed T-Shirt','Spykar Mens Half Sleeve Round Neck Printed T-Shirt','2018-09-24 08:30:30','0000-00-00 00:00:00'),(72,'ts6','1','T-shirts','999','Mustang Mens Circular Knit Prinetd Short Sleeves T-Shirt','Mustang Mens Circular Knit Prinetd Short Sleeves T-Shirt','2018-09-24 08:30:42','0000-00-00 00:00:00'),(73,'jeans1','1','jeans','2399','Mustang Mens New Oregon Fit Denim','Mustang Mens New Oregon Fit Denim','2018-09-24 08:30:49','0000-00-00 00:00:00'),(74,'jeans2','1','jeans','1699','Flying Machine Mens Regular Fit Bruce Denim','Flying Machine Mens Regular Fit Bruce Denim','2018-09-24 08:30:57','0000-00-00 00:00:00'),(75,'jeans3','1','jeans','1999','United Colors of Benetton Mens Slim Fit Denim','United Colors of Benetton Mens Slim Fit Denim','2018-09-24 08:31:07','0000-00-00 00:00:00'),(76,'jeans4','1','jeans','2199','Mustang Mens Michigan Fit Denim','Mustang Mens Michigan Fit Denim','2018-09-24 08:31:15','0000-00-00 00:00:00'),(77,'jeans5','1','jeans','1599','Flying Machine Mens Slim Fit Eddie Denim','Flying Machine Mens Slim Fit Eddie Denim','2018-09-24 08:31:23','0000-00-00 00:00:00'),(78,'jeans6','1','jeans','2599','Levis Mens 504 Fit Tapered Denim','Levis Mens 504 Fit Tapered Denim','2018-09-24 08:31:34','0000-00-00 00:00:00'),(79,'shoe1','1','Footwear','3999','Enroute Men\'s Footwear','Enroute Men\'s Footwear','2018-09-24 08:31:45','0000-00-00 00:00:00'),(80,'shoe2','1','Footwear','4499','Enroute Men\'s Footwear','Enroute Men\'s Footwear','2018-09-24 08:31:53','0000-00-00 00:00:00'),(82,'shoe3','1','Footwear','3999','Enroute Men\'s Footwear','Enroute Men\'s Footwear','2018-09-24 08:32:01','0000-00-00 00:00:00'),(83,'shoe4','1','Footwear','1676','Franco Leone - Men\'s Shoes','Franco Leone - Men\'s Shoes','2018-09-24 08:32:11','0000-00-00 00:00:00'),(84,'shoe5','1','Footwear','2195','Franco Leone - Men\'s Shoes','Franco Leone - Men\'s Shoes','2018-09-24 08:32:36','0000-00-00 00:00:00'),(85,'shoe6','1','Footwear','1895','Franco Leone - Men\'s Shoes','Franco Leone - Men\'s Shoes','2018-09-24 08:32:20','0000-00-00 00:00:00'),(86,'w1','1','watches','3650','Polo Club Men\'s Watch','Polo Club Men\'s Watch','2018-09-22 07:14:12','0000-00-00 00:00:00'),(87,'w2','1','watches','1750','Polo Club Men\'s Watch','Polo Club Men\'s Watch','2018-09-22 07:14:35','0000-00-00 00:00:00'),(88,'w3','1','watches','1750','Polo Club Men\'s Watch','Polo Club Men\'s Watch','2018-09-22 07:14:45','0000-00-00 00:00:00'),(89,'w4','1','watches','1569','Polo Club Men\'s Watch','Polo Club Men\'s Watch','2018-09-24 08:32:45','0000-00-00 00:00:00'),(90,'w5','1','watches','1999','Polo Club Men\'s Watch','Polo Club Men\'s Watch','2018-09-24 08:32:54','0000-00-00 00:00:00'),(91,'w6','1','watches','1349','Polo Club Men\'s Watch','Polo Club Men\'s Watch','2018-09-24 08:33:25','0000-00-00 00:00:00'),(92,'sh1','1','Shorts','1799','Mustang Mens Casual Shorts','Mustang Mens Casual Shorts','2018-09-24 08:33:39','0000-00-00 00:00:00'),(93,'sh2','1','Shorts','1299','Killer Mens 8 Pocket Checks Shorts','Killer Mens 8 Pocket Checks Shorts','2018-09-24 08:33:45','0000-00-00 00:00:00'),(94,'sh3','1','Shorts','1295','Wrangler Mens Spencer Denim Shorts','Wrangler Mens Spencer Denim Shorts','2018-09-24 08:33:53','0000-00-00 00:00:00'),(95,'sh4','1','Shorts','1299','Killer Mens 8 Pocket Checks Shorts','Killer Mens 8 Pocket Checks Shorts','2018-09-24 08:34:01','0000-00-00 00:00:00'),(96,'sh5','1','Shorts','2195','Wrangler Mens Cargo Fit Checks Shorts','Wrangler Mens Cargo Fit Checks Shorts','2018-09-24 08:34:08','0000-00-00 00:00:00'),(97,'sh6','1','Shorts','1699','Indian Terrain Mens Regular Fit Cargo Shorts','Indian Terrain Mens Regular Fit Cargo Shorts','2018-09-24 08:34:18','0000-00-00 00:00:00'),(98,'k7','2','Kurtas','2099','Stop Classic Mix and Match Short Kurta','Stop Classic Mix and Match Short Kurta','2018-09-22 12:17:27','2018-09-22 12:17:27'),(99,'k8','2','Kurtas','1299','Beautiful Classic Mix and Match Short Kurta','Beautiful Classic Mix and Match Short Kurta','2018-09-22 12:17:13','2018-09-22 12:17:13'),(100,'foot7','2','Sandals','1999','Tresmode Ladies Pump Shoes','Tresmode Ladies Pump Shoes','2018-09-22 12:31:37','2018-09-22 12:31:37'),(101,'Jewellery7','2','Artificial Jewellery','14999','Beautiful Classic ring','Beautiful Classic ring','2018-09-23 08:01:18','2018-09-23 08:01:18');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `itemno` varchar(30) NOT NULL,
  `price` varchar(30) NOT NULL,
  `quantity` int(10) NOT NULL,
  `uname` varchar(30) NOT NULL,
  `ac_no` varchar(30) NOT NULL,
  `mob_no` varchar(30) NOT NULL,
  `add` varchar(300) NOT NULL,
  `bank` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `order_no` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (2,'shoe1','3999',4,'dfsg','65454545','9458485085','Kasganj','axis bank','Kasganj','ord-472','2018-09-23 15:44:37','2018-09-23 15:44:37'),(3,'d1','1899',1,'Rahul','87878787878','9458485085','Kasganj','allahabad bank','Kasganj','ord-420','2018-09-23 15:46:20','2018-09-23 15:46:20'),(5,'18','1690',1,'ww','54545','9458485085','Kasganj','axis bank','Kasganj','ord-420','2018-09-23 15:49:42','2018-09-23 15:49:42'),(11,'5','1999',1,'gfh','5454564','9458485085','Kasganj','allahabad bank','Kasganj','ord-218','2018-09-23 16:07:07','2018-09-23 16:07:07'),(12,'Toy1','525',1,'gfdh','87878787878','9458485085','Kasganj','axis bank','Kasganj','ord-121','2018-09-23 16:08:12','2018-09-23 16:08:12'),(15,'5','1999',1,'rahul','8787878787','8789898989','address','bank of baroda','jhjk','ord-266','2018-09-23 16:45:34','2018-09-23 16:45:34'),(16,'jeans1','2399',1,'ra','8788','4522','aaaaaa','bank of india','aaaa','ord-429','2018-09-23 16:48:22','2018-09-23 16:48:22'),(17,'shoe1','3999',1,'raaaaa','11111111','2222222','ccccccc','axis bank','aaaaaaa','ord-227','2018-09-23 16:50:57','2018-09-23 16:50:57'),(18,'shoe1','3999',1,'tty','4444444','777777777','pppppppppp','allahabad bank','aaaaaaaaaa','ord-164','2018-09-23 16:57:43','2018-09-23 16:57:43'),(19,'s2','2599',2,'tty','4444444','777777777','pppppppppp','allahabad bank','aaaaaaaaaa','ord-104','2018-09-23 16:57:43','2018-09-23 16:57:43'),(20,'shoe1','3999',1,'yuuu','8888','87878787','sdfkl','bank of india','fdlg','ord-213','2018-09-23 16:58:10','2018-09-23 16:58:10');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registers`
--

DROP TABLE IF EXISTS `registers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registers` (
  `id` int(10) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `gen` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `add` varchar(300) NOT NULL,
  `city` varchar(30) NOT NULL,
  `coun` varchar(30) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registers`
--

LOCK TABLES `registers` WRITE;
/*!40000 ALTER TABLE `registers` DISABLE KEYS */;
INSERT INTO `registers` VALUES (1,'Rahul','Maheshwari','male','rahul123','9458485085','Bilram Gate','Kasganj','India','17-09-1992','2018-09-23 16:54:55','0000-00-00 00:00:00'),(5,'rahul','mah','Male','Rahul@123','9458485085','Kasganj','Kasganj','India','17-07-1992','2018-09-23 11:26:06','2018-09-23 11:26:06');
/*!40000 ALTER TABLE `registers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` varchar(30) NOT NULL,
  `subcategory` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategories`
--

LOCK TABLES `subcategories` WRITE;
/*!40000 ALTER TABLE `subcategories` DISABLE KEYS */;
INSERT INTO `subcategories` VALUES (1,'1','Casual Shirts','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'1','jeans','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'1','T-shirts','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'1','Footwear','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'1','Shorts','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'1','watches','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'2','Dresses','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'2','Churidar Suits','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'2','Kurtas','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'2','Sandals','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'2','Office Wear','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'2','Artificial Jewellery','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'3','Baby Apparel','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'3','Girls Apparel','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'3','Boys Apparel','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'3','Kids Toys','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `subcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trashes`
--

DROP TABLE IF EXISTS `trashes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trashes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `itemno` varchar(30) NOT NULL,
  `catg` varchar(50) NOT NULL,
  `subcatg` varchar(50) NOT NULL,
  `price` varchar(30) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `info` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trashes`
--

LOCK TABLES `trashes` WRITE;
/*!40000 ALTER TABLE `trashes` DISABLE KEYS */;
INSERT INTO `trashes` VALUES (1,'cs1','men','Casual Shirts','400','ahakaaajaj','28-06-11 04-56-14','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'cs3','men','Casual Shirts','500','haioaajalkaj','28-06-11 04-57-11','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'d1','women','Dresses','5700','ghsfshsshs','28-06-11 05-00-46','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'d2','women','Dresses','45666','yrsysfysus','28-06-11 05-00-46','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'cs1','men','Casual Shirts','7635635','qtyqwtywrtyw','28-06-11 05-06-40','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'cs2','men','Casual Shirts','53674','deuyddydtg','28-06-11 05-11-33','0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'cs1','men','Casual Shirts','355','sjksjs','29-06-11 04-34-39','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'33','men','Casual Shirts','444','sxxddxd','29-06-11 04-35-16','0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'cs2','men','Casual Shirts','3434','hdgdjgdjg','29-06-11 04-35-30','0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'34','men','Casual Shirts','0','ddddd','29-06-11 04-42-31','0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'45','men','Casual Shirts','0','xxxxxx','29-06-11 04-43-02','0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'cs1','men','Casual Shirts','345','sssssssssssss','29-06-11 04-43-02','0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'23','1','Casual Shirts','2333','wwwwwwwwwwwwww','30-06-11 04-56-19','0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'cs1','1','Casual Shirts','23','aaaaaaaaaaaaa','30-06-11 04-56-52','0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,'34','1','Casual Shirts','0','wwwwwwwwwwwwww','30-06-11 04-56-52','0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'j16','1','jeans','458','sadfj','erwgf','2018-09-24 02:20:16','2018-09-24 02:20:16'),(37,'j17','1','jeans','7678','sdjtg','ergrgwe','2018-09-24 02:20:16','2018-09-24 02:20:16'),(38,'j18','1','jeans','9898','yugujkh','kljhljh','2018-09-24 02:20:16','2018-09-24 02:20:16'),(39,'103','1','jeans','1000','kjhkj','kjhlkj','2018-09-24 06:13:47','2018-09-24 06:13:47'),(40,'104','1','jeans','8','ghgyu','jhgjh','2018-09-24 06:13:48','2018-09-24 06:13:48'),(41,'105','1','jeans','89','lkjghh','jkgkjhftfty','2018-09-24 08:04:46','2018-09-24 08:04:46'),(42,'107','1','jeans','1233','sgdrtr','rtgywet','2018-09-24 08:04:46','2018-09-24 08:04:46'),(43,'106','1','jeans','8787','sdffgfjh','jfdsgdklj','2018-09-24 08:04:46','2018-09-24 08:04:46');
/*!40000 ALTER TABLE `trashes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Rahul','rahul@gmail.com','$2y$10$TnlrOew6uscqevs0bnCi5O4SF344ZBi5j6exK8AeDNwG0YWTHooLa','2018-09-23 22:39:09','0000-00-00 00:00:00'),(5,'rahul mah','rahulmah438@gmail.com','$2y$10$ePvnRnrqvGd39jMA3RPGDOO6E/M/Fspu2umjCb8vtKOnc4M7ny3uK','2018-09-23 11:26:06','2018-09-23 11:26:06'),(6,'admin','admin@admin.com','$2y$10$TnlrOew6uscqevs0bnCi5O4SF344ZBi5j6exK8AeDNwG0YWTHooLa','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-25 20:41:38
