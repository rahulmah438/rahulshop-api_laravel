<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Item;
use App\Trash;
use App\Http\Resources\Item as ItemResource;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        // get categories
        $items = Item::where('subcatg', $type)->get();

        // return collection of categories as a resource
        return ItemResource::collection($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->isMethod('put')) {
            $item = Item::findOrFail($request->itemno);
            $item->itemno = $request->itemno;
            $item->catg = $item->catg;
            $item->subcatg = $item->subcatg;
            $item->price = $request->input('price');
            $item->desc = $request->input('desc');
            $item->info = $request->input('info');
            if($item->save()){
                return new ItemResource($item);
            }
        }
        else {   
            $item = new Item;
            $item->itemno = $request->input('itemno');
            $item->catg = $request->input('catg');
            $item->subcatg = $request->input('subcatg');
            $item->price = $request->input('price');
            $item->desc = $request->input('desc');
            $item->info = $request->input('info');
            if($item->save()){
                return new ItemResource($item);
            }
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         // get single itewm
         $item = Item::findOrFail($id);

         // return item as a resource
         return new ItemResource($item);
    }


    public function showItem($itemno)
    {
         // get single itewm
         $item = Item::where('itemno', $itemno)->first();

         // return item as a resource
         return new ItemResource($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get single itewm
        $item = Item::where('itemno', $id)->first();

        // delete the item
        if($item->delete()) {
            $trash = new Trash;
            $trash->itemno = $item->itemno;
            $trash->catg = $item->catg;
            $trash->subcatg = $item->subcatg;
            $trash->price = $item->price;
            $trash->desc = $item->desc;
            $trash->info = $item->info;
            if($trash->save()) {
                return new ItemResource($item);
            }
        }
    }
}
