<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Trash extends Authenticatable 
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'itemno', 'catg', 'subcatg', 'price', 'info', 'desc',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    
}
