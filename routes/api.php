<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//for listing all categories
Route::get('categories', 'CategoryController@index');
// for show single category
Route::get('category/{id}', 'CategoryController@show');
// for create new category
Route::post('category', 'CategoryController@store');
// for update category
Route::put('category', 'CategoryController@store');
// for delete category
Route::delete('category/{id}', 'CategoryController@destroy');


//for listing all items of type something
Route::get('items/{type}', 'ItemController@index');
// for show single item
Route::get('item/{id}', 'ItemController@show');
// for show single item by itemno
Route::get('itemno/{itemno}', 'ItemController@showItem');
// for create new item
Route::post('item', 'ItemController@store');
// for update item
Route::put('item', 'ItemController@store');
// for delete category
Route::delete('item/{id}', 'ItemController@destroy');

// for create new user
Route::post('register', 'RegisterController@store');

//for listing all orders
Route::get('orders', 'OrderController@index');
// for create new order
Route::post('order', 'OrderController@store');

//for listing all feedbacks
Route::get('feedbacks', 'FeedbackController@index');
//for create new feedback
Route::post('feedback', 'FeedbackController@store');

//for listing all subcategories of related category
Route::get('subcategories/{category}', 'SubcategoryController@index');
//for listing all subcategories 
Route::get('subcategories', 'SubcategoryController@fullIndex');


Route::group([

    'prefix' => 'auth'

], function () {
    // Route::post('login', [ 'as' => 'login', 'uses' => 'AuthController@login']);
   Route::post('login', 'AuthController@login')->name('login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});